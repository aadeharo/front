import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router'
import { MatFormFieldModule } from '@angular/material/form-field'
import { NgForm } from '@angular/forms'
import { ServiceFrontService } from '../../servicios/serviceFront/serviceFront.service'


@Component({ 
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})

export class LoginComponent implements OnInit {


  constructor(private servicioFront: ServiceFrontService, private router: Router) { }

  ngOnInit() { }

}