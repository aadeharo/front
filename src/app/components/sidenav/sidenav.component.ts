import { Component } from '@angular/core';
import { BreakpointObserver, Breakpoints } from '@angular/cdk/layout';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { ServiceFrontService } from '../../servicios/serviceFront/serviceFront.service'

@Component({
  selector: 'app-sidenav',
  templateUrl: './sidenav.component.html',
  styleUrls: ['./sidenav.component.css']
})
export class SidenavComponent {

  isHandset: Observable<boolean>

  constructor( private breakpointObserver: BreakpointObserver, private servicioFront : ServiceFrontService) {

     this.isHandset = this.breakpointObserver.observe(Breakpoints.Handset)
    .pipe(
      map(result => result.matches)
    ); 

  }

  ngOnInit() { }

}

