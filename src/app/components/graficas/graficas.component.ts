import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { GeneradorReportesService } from '../../servicios/generadorReportes/generadorReportes.service'
import { ServiceBackService } from '../../servicios/serviceBack/serviceBack.service'
import { MatPaginator, MatSortModule, MatTableDataSource } from '@angular/material';
import { ActivatedRoute } from "@angular/router";

//import { Chart } from 'chart.js'
//import { PdfAlignmentStyle } from '@syncfusion/ej2-pdf-export';

@Component({
  selector: 'app-graficas',
  template: `<ejs-chart id='chart-container'></ejs-chart>`,
  templateUrl: './graficas.component.html',
  styleUrls: ['./graficas.component.css'],
  providers: [MatPaginator, MatSortModule, MatTableDataSource],
  encapsulation: ViewEncapsulation.None
})

export class GraphicsComponent implements OnInit {

  //calendario
  start: Date
  end: Date

  startD: Date = new Date
  endD: Date

  datosClientes: any

  page: Number = 1;
  count: number = 5

  constructor(private activatedRouted: ActivatedRoute, private sort: MatSortModule, private generadorReportes: GeneradorReportesService, private servicioBack: ServiceBackService) {

    this.activatedRouted.params.subscribe(params => {
      this.servicioBack.getDatosClientes()
        .subscribe(data => {
          console.log(data)
          this.datosClientes = data.list
        },
          error => {
            console.log("fallo el call de la API");
          });
    })
  }

  ngOnInit() {
    /* no borrar

      this.servicioBack.getDatosClientes()
        .subscribe(data => {
         if(JSON.stringify(this.jsonData)!=[]){
           this.jsonData=data;
           this.jsonData.forEach( res => {
              this.idSize.push(res.value);
           });
        }
    }) */
  }



  // events on slice click
  chartClicked(e: any): void {
    console.log(e);
  }

  // event on pie chart slice hover
  chartHovered(e: any): void {
    console.log(e);
  }

  //Exportar excel
  //Revisar el tema de como los exporta, estan desordenados
  exportAsXLSX(): void {
    this.generadorReportes.exportAsExcelFile(this.datosClientes, 'Reporte');
  }



}
