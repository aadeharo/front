import { RouterModule, Routes } from '@angular/router';

import { HomeComponent } from './components/home/home.component';
import { LoginComponent } from './components/login/login.component'
import { GraphicsComponent } from './components/graficas/graficas.component'

const routes: Routes = [
    { path: 'home', component: HomeComponent },
    { path: 'login', component: LoginComponent },
    { path: 'graficas', component: GraphicsComponent },
    { path: '**', pathMatch:'full', redirectTo: 'home' }  
    
];
 
export const appRouting = RouterModule.forRoot(routes);