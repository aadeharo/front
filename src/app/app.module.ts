import { BrowserModule } from '@angular/platform-browser';
import { AppComponent } from './app.component';

import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms'

import { ChartsModule } from 'ng2-charts'
import { ChartModule } from '@syncfusion/ej2-angular-charts'
import { CategoryService } from '@syncfusion/ej2-angular-charts'
import { LegendService } from '@syncfusion/ej2-angular-charts'
import { TooltipService } from '@syncfusion/ej2-angular-charts'
import { DataLabelService } from '@syncfusion/ej2-angular-charts'
import { LineSeriesService } from '@syncfusion/ej2-angular-charts'

import { DatePickerModule } from '@syncfusion/ej2-angular-calendars'
import { DateRangePickerModule } from '@syncfusion/ej2-angular-calendars'

import { MatTableModule } from '@angular/material/table';

import { HttpModule } from '@angular/http';
import { appRouting } from './app.routes';

import { GraphicsComponent } from './components/graficas/graficas.component';
import { SidenavComponent } from './components/sidenav/sidenav.component';
import { HomeComponent } from './components/home/home.component';
import { LoginComponent } from './components/login/login.component';
 
import { ServiceBackService } from './servicios/serviceBack/serviceBack.service'
import { ServiceFrontService } from './servicios/serviceFront/serviceFront.service';
import { GeneradorReportesService } from './servicios/generadorReportes/generadorReportes.service'

import { LayoutModule } from '@angular/cdk/layout';

import { MatToolbarModule } from '@angular/material/toolbar';
import { MatButtonModule } from '@angular/material/button';
import { MatSidenavModule } from '@angular/material/sidenav';
import { MatIconModule } from '@angular/material/icon';
import { MatListModule } from '@angular/material/list'
import { BrowserAnimationsModule } from '@angular/platform-browser/animations'
import { MatCheckboxModule } from '@angular/material/checkbox';
import { MatFormFieldModule } from '@angular/material/form-field'
import { MatInputModule } from '@angular/material/input';
import { MDBBootstrapModule } from 'angular-bootstrap-md';
import { MatProgressSpinnerModule, MatSortModule } from '@angular/material'
import { NgxPaginationModule } from 'ngx-pagination'

import { NgbModule } from '@ng-bootstrap/ng-bootstrap'

@NgModule({
  declarations: [
    AppComponent,

    HomeComponent,
    LoginComponent,
    GraphicsComponent,
    SidenavComponent,
  ],

  imports: [
    BrowserModule,
    appRouting,
    FormsModule,
    ChartsModule,
    DatePickerModule,
    DateRangePickerModule,
    HttpModule,
    LayoutModule,
    MatToolbarModule,
    MatButtonModule,
    MatSidenavModule,
    MatIconModule,
    MatListModule,
    BrowserAnimationsModule,
    MatCheckboxModule,
    MatFormFieldModule,
    MatInputModule,
    ChartModule,
    MatTableModule,
    MatProgressSpinnerModule,
    MatSortModule,
    NgbModule,
    MDBBootstrapModule.forRoot(),
    NgxPaginationModule
  ],

  providers: [
    
    ServiceBackService,
    ServiceFrontService,
    GeneradorReportesService,

    CategoryService,
    LegendService,
    TooltipService,
    DataLabelService,
    LineSeriesService
  ],
  exports: [
    MatTableModule,
    MatSortModule
 ],

  bootstrap: [AppComponent],
  
})

export class AppModule { }
