import { Injectable } from '@angular/core';
import { Http, Headers } from '@angular/http';
import 'rxjs/Rx';
import 'rxjs/add/operator/map'


@Injectable({
  providedIn: 'root'
})
export class ServiceBackService {

  datosAplicacion : any

  constructor(private http: Http) {
    console.log("Servicio Back listo");
  }

  getDatosClientes() {
    console.log("Llamado a API que devuelve datos de clientes");

    let header = new Headers({ 'Content-Type': 'application/json' });
    //let datosAppURL = "http://10.70.2.134:8080/api/clientes"           //IP Citrix
    let datosAppURL = "http://localhost:8080/prueba/clientes";

    return this.http.get(datosAppURL, { headers: header })
      .map(dat => {
        console.log(dat.json());
        console.log("Llamado positivo a API que devuelve datos de clientes")
        this.datosAplicacion = dat.json().list;
        return dat.json();
      },
        err => console.log("error: " + err.json())
      );
  }


}
