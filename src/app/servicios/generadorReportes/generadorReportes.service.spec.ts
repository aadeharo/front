import { TestBed } from '@angular/core/testing';

import { GeneradorReportesService } from './generadorReportes.service';

describe('GeneradorReportesService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: GeneradorReportesService = TestBed.get(GeneradorReportesService);
    expect(service).toBeTruthy();
  });
});
