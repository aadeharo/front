import { TestBed } from '@angular/core/testing';

import { ServiceFrontService } from './serviceFront.service';

describe('ServiceFrontService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: ServiceFrontService = TestBed.get(ServiceFrontService);
    expect(service).toBeTruthy();
  });
});
